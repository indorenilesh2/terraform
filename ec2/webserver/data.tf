data "aws_subnet" "dev-subnet" {
filter {
    name   = "tag:Name"
    values = ["DEV Public Subnet (AZ1)"]
}    
}

#output "devsubnet" {
#    value = data.aws_subnet.dev-subnet.id
#}

data "aws_security_group" "dev-sg" {
        name = "DEV Public SG"
}

#output "devsg" {
#    value = data.aws_security_group.dev-sg
#}

locals {
    subnet_id = {
        DEV = data.aws_subnet.dev-subnet.id
    }

    security_groups = {
        DEV = [data.aws_security_group.dev-sg.id]
  }
}