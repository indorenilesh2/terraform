locals {
common_tags = {
    Application = var.appname
    Owner = var.teamdl
    Project = var.project
    Env = var.env
    ManagedBy   = "Terraform"
    sdawsr = "HMT9090"
    }
}